///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////

// KinematicFitTool includes

#include "KinematicFitTool/KinematicFitTool.h"
#include "PathResolver/PathResolver.h"

#include <TFile.h>
#include <cmath>

// Initialize the static instance pointer to nullptr
KinematicFitTool* KinematicFitTool::s_instance = nullptr;

KinematicFitTool::KinematicFitTool(const std::string &name)
    : asg::AsgTool(name)
{
  s_instance = this;
}

StatusCode KinematicFitTool::initialize()
{
  const char* DetectPart[4] = {"Barrel", "Crack", "Endcap", "NoTrack"};

  std::string DataTakingPeriod = m_isRun3 ? "Run3" : "Run2";

  std::string EnRespFilename =
    PathResolverFindCalibFile("KinematicFitTool/E_" + m_JetAlgo +
			      "_TaOgataParameters_" + DataTakingPeriod + "_FastSim_PeakCentered.root");
  std::string PtRespFilename =
    PathResolverFindCalibFile("KinematicFitTool/pT_" + m_JetAlgo +
			      "_TaOgataParameters_" + DataTakingPeriod + "_FastSim_PeakCentered.root");
  std::string pXconstrFilename =
    PathResolverFindCalibFile("KinematicFitTool/pXconstr_" + m_JetAlgo +
			      "_TaOTaParameters.root");
  std::string pYconstrFilename =
    PathResolverFindCalibFile("KinematicFitTool/pYconstr_" + m_JetAlgo +
			      "_TaOTaParameters.root");

  std::unique_ptr<TFile> EnRespFile =
    std::make_unique<TFile>(EnRespFilename.c_str(), "READ");
  std::unique_ptr<TFile> PtRespFile =
    std::make_unique<TFile>(PtRespFilename.c_str(), "READ");
  std::unique_ptr<TFile> pXconstrFile =
    std::make_unique<TFile>(pXconstrFilename.c_str(), "READ");
  std::unique_ptr<TFile> pYconstrFile =
    std::make_unique<TFile>(pYconstrFilename.c_str(), "READ");

  for (int i=0; i<4; i++){
    // ------------- Parameter tensors TF inizialization ----------------- //
    for (int j=0; j<6; j++){
      TString objName = Form("TransferFunction_%s_%1.1f_%1.1f_ln(pT[GeV])",
			     DetectPart[i], m_logPT[j], m_logPT[j+1]);
      m_par_tensor_E[i][j] = *(std::vector<double>*)(EnRespFile->Get(objName));
      m_par_tensor_pT[i][j] = *(std::vector<double>*)(PtRespFile->Get(objName));
    }

    // ------------- Parameter tensors inizialization - pXpY constraint ----------------- //
    std::string prefix = std::to_string(i) + "_AdditionalJets";
    m_par_tensor_pXconstr[i]=
      *(std::vector<double>*)(pXconstrFile->Get((prefix+"_pX").c_str()));
    m_par_tensor_pYconstr[i]=
      *(std::vector<double>*)(pYconstrFile->Get((prefix+"_pY").c_str()));
  }

  EnRespFile->Close();
  PtRespFile->Close();
  pXconstrFile->Close();
  pYconstrFile->Close();

  return StatusCode::SUCCESS;
}

StatusCode KinematicFitTool::applyKF(const xAOD::PhotonContainer& photons,
				     xAOD::JetContainer& jets,
                                     double& KF_Mbb)
{
  // Selections for early exit
  if(photons.size() < 2) return StatusCode::SUCCESS;
  if(jets.size() < 2) return StatusCode::SUCCESS;

  const xAOD::Photon* photon1 = photons.at(0);
  const xAOD::Photon* photon2 = photons.at(1);

  static const SG::AuxElement::ConstAccessor<char> btagwp(m_BtaggingWP);

  xAOD::Jet* bjet1 = nullptr;
  xAOD::Jet* bjet2 = nullptr;
  std::vector<xAOD::Jet*> extra_jets;
  int nCentralJets = 0;
  for(xAOD::Jet* jet : jets){
    if(jet->pt() < m_Jet_Min_Pt) continue;

    if(btagwp(*jet) && std::abs(jet->eta())<2.5){
      if(!bjet1) bjet1 = jet;
      else if(!bjet2) bjet2 = jet;
      else extra_jets.emplace_back(jet);
    }
    else extra_jets.emplace_back(jet);

    if(std::abs(jet->eta())<2.5) nCentralJets++;
  }
  if(!bjet2) return StatusCode::SUCCESS;
  if(nCentralJets>=6) return StatusCode::SUCCESS;
  if(extra_jets.size()>=14) return StatusCode::SUCCESS; // TMinuit max pars limit check (50): 14 extra jets (+2 cand jets) = 48 pars
  
  m_Event.photon1 = photon1;
  m_Event.photon2 = photon2;
  m_Event.bjet1 = bjet1;
  m_Event.bjet2 = bjet2;
  m_Event.extra_jets = extra_jets;

  // Selections passed run KF
  int npar = m_isFixAngles ? 2 + extra_jets.size() : 6 + 3 * extra_jets.size();

  // 2 iterations
  for (int i = 1; i <= 2; i++){
    std::unique_ptr<TMinuit> minuit = std::make_unique<TMinuit>(npar);
    int conv = minuit->SetPrintLevel(-1);
    m_Event.iteration = i;
    minuit->SetFCN(&KinematicFitTool::FCN);
    minuit->SetErrorDef(0.5);
    std::unique_ptr<double[]> arglist = std::make_unique<double[]>(npar);
    arglist[0] = 2;
    minuit->mnexcm("SET STR", arglist.get(), 1, m_minuit_ierflg);
    SetParameterNameAndRange(minuit.get(), bjet1, bjet2, extra_jets);
    arglist[0] = 50000.;
    arglist[1] = 1.;
    minuit->mnexcm("MIGRAD", arglist.get(), 2, conv);
    ShareFit(minuit.get(), bjet1, bjet2, extra_jets, KF_Mbb, i);
  }

  return StatusCode::SUCCESS;
}


double KinematicFitTool::TaOgataResponse(const TLorentzVector& FitJet,
					 const xAOD::Jet* Jet, bool EorPT){
  int row = -1;
  if(std::abs(FitJet.Eta())<1.37) row = 0;
  else if(std::abs(FitJet.Eta())<1.52) row = 1;
  else if(std::abs(FitJet.Eta())<2.5) row = 2;
  else if(std::abs(static_cast<float>(FitJet.Eta()))<=4.5) row = 3;

  double ln_pT = std::log(FitJet.Pt()/Athena::Units::GeV);
  // Use std::lower_bound to find the first element not less than ln_pT
  auto it = std::lower_bound(m_logPT.begin(), m_logPT.end(), ln_pT);
  int col = static_cast<int>(it - m_logPT.begin()) - 1;
  // Handle edge cases
  if (col < 0) col = 0;
  int nbins = m_logPT.size();
  if (col >= nbins - 1) col = nbins - 2;

  double x = EorPT ? (FitJet.E()-Jet->e())/FitJet.E() :
    (FitJet.Pt()-Jet->pt())/FitJet.Pt();
  std::vector<double> par =
    (EorPT ? m_par_tensor_E : m_par_tensor_pT)[row][col];

  double result = par.at(8) *
    std::pow(std::atan( par.at(0) * (x-par.at(1))) + 0.5*M_PI, par.at(2)) *
    std::exp(-0.5 * std::pow( (x-par.at(3)) / par.at(4) , 2)) *
    std::pow(std::atan(-par.at(5) * (x-par.at(6))) + 0.5*M_PI, par.at(7));
  return result;
}


double KinematicFitTool::MomConstraint(double FitJetp, int nAddJets, bool PxOrPy){
  double x = FitJetp;
  std::vector<double> par =
    (PxOrPy ? m_par_tensor_pXconstr : m_par_tensor_pYconstr)[nAddJets];

  double result = par.at(6) *
    std::pow(std::atan( par.at(0) * (x-par.at(1))) + 0.5*M_PI, par.at(2)) *
    std::pow(std::atan(-par.at(3) * (x-par.at(4))) + 0.5*M_PI, par.at(5));
  return result;
}

void KinematicFitTool::FCN(int& /*npar*/, double* /*grad*/, double& LH, double* par, int /*flag*/){
  if (s_instance) s_instance->FCNImpl(LH, par);
}


void KinematicFitTool::FCNImpl(double& LH, double* par){
  TLorentzVector FitBJet1, FitBJet2;
  FitBJet1.SetPtEtaPhiE(par[0] * m_Event.bjet1->pt(),
			par[2], par[4], par[0] * m_Event.bjet1->e());
  FitBJet2.SetPtEtaPhiE(par[1] * m_Event.bjet2->pt(),
			par[3], par[5], par[1] * m_Event.bjet2->e());

  std::vector<TLorentzVector> AddFitJets;
  AddFitJets.resize(m_Event.extra_jets.size());
  for(unsigned int i = 0; i < m_Event.extra_jets.size(); i++){
    AddFitJets[i].SetPtEtaPhiE
      (par[6+i*3] * m_Event.extra_jets.at(i)->pt(),
       par[7+i*3], par[8+i*3], par[6+i*3] * m_Event.extra_jets.at(i)->e());
  }

  LH = 0.;
  // ------- Control Condition for Rare Nan Parameter cases ------- //
  for(int i = 0; i < 4; i++){
    if(std::isnan(FitBJet1[i]) || std::isnan(FitBJet2[i])){
      LH = std::numeric_limits<double>::infinity();
      return;
    }
    for(const auto& addfitJet : AddFitJets){
      if(std::isnan(addfitJet[i])){
	LH = std::numeric_limits<double>::infinity();
	return;
      }
    }
  }

  LH += -2*std::log(TaOgataResponse(FitBJet1, m_Event.bjet1, true));
  LH += -2*std::log(TaOgataResponse(FitBJet2, m_Event.bjet2, true));

  LH += -2*std::log(TaOgataResponse(FitBJet1, m_Event.bjet1, false));
  LH += -2*std::log(TaOgataResponse(FitBJet2, m_Event.bjet2, false));

  TLorentzVector Photon1 = m_Event.photon1->p4();
  TLorentzVector Photon2 = m_Event.photon2->p4();

  double PxHH = (FitBJet1 + FitBJet2 + Photon1 + Photon2).Px();
  double PyHH = (FitBJet1 + FitBJet2 + Photon1 + Photon2).Py();

  int nCentralJets = 0;
  for(unsigned int i=0; i<m_Event.extra_jets.size(); i++){
    LH += -2*std::log(TaOgataResponse(AddFitJets[i],
				      m_Event.extra_jets.at(i), true));
    LH += -2*std::log(TaOgataResponse(AddFitJets[i],
				      m_Event.extra_jets.at(i), false));
    PxHH += AddFitJets[i].Px();
    PyHH += AddFitJets[i].Py();
    if( std::abs((m_Event.extra_jets.at(i))->eta()) < 2.5 ) nCentralJets++;
  }

  static const double lambda = 3.05;
  LH += -2*lambda*std::log(MomConstraint(PxHH/Athena::Units::GeV, nCentralJets, true));
  LH += -2*lambda*std::log(MomConstraint(PyHH/Athena::Units::GeV, nCentralJets, false));

  static const double lambda_m = 0.1;
  if(m_Event.iteration==2)
    LH += lambda_m * std::pow((FitBJet1+FitBJet2).M()/Athena::Units::GeV - 125., 2);

  return;
}


void KinematicFitTool::SetParameterNameAndRange
(TMinuit* m, const xAOD::Jet* bjet1, const xAOD::Jet* bjet2,
 const std::vector<xAOD::Jet*>& extra_jets){

  m->mnparm(0, "Scaling_jet1", 1.0, 0.01, 0.1, 2.0, m_minuit_ierflg);
  m->mnparm(1, "Scaling_jet2", 1.0, 0.01, 0.1, 2.0, m_minuit_ierflg);
  // angular terms not included in KF but kept for possible other studies
  m->mnparm(2, "EtaFit_jet1", bjet1->eta(), .01*bjet1->eta(),
	    -4.4, 4.4, m_minuit_ierflg);
  m->mnparm(3, "EtaFit_jet2", bjet2->eta(), .01*bjet2->eta(),
	    -4.4, 4.4, m_minuit_ierflg);

  m->mnparm(4, "PhiFit_jet1", bjet1->phi(), .01*bjet1->phi(),
	    -M_PI, M_PI, m_minuit_ierflg);
  m->mnparm(5, "PhiFit_jet2", bjet2->phi(), .01*bjet2->phi(),
	    -M_PI, M_PI, m_minuit_ierflg);

  if(m_isFixAngles){
    for(unsigned int i=2; i<6; i++) m->FixParameter(i);
  }

  for(unsigned int i = 0; i < extra_jets.size(); i++){
    m->mnparm(6+i*3, "Scaling_jet"+std::to_string(i+3), 1.0, 0.01, 0.1, 2.0, m_minuit_ierflg);
    m->mnparm(7+i*3, "EtaFit_jet"+std::to_string(i+3),
	      extra_jets.at(i)->eta(), .01*extra_jets.at(i)->eta(),
	      -4.4, 4.4, m_minuit_ierflg);
    m->mnparm(8+i*3, "PhiFit_jet"+std::to_string(i+3),
	      extra_jets.at(i)->phi(), .01*extra_jets.at(i)->phi(),
	      -M_PI, M_PI, m_minuit_ierflg);

    if(m_isFixAngles){
      m->FixParameter(7+i*3);
      m->FixParameter(8+i*3);
    }
  }

}


void KinematicFitTool::ShareFit(const TMinuit* m,
				xAOD::Jet* bjet1, xAOD::Jet* bjet2,
				std::vector<xAOD::Jet*>& extra_jets,
				double& KF_Mbb, int iteration){
  double eta, phi, alpha, err, E, pt, jet_mass;
  m->GetParameter(0, alpha, err);
  m->GetParameter(2, eta, err);
  m->GetParameter(4, phi, err);
  pt = alpha * m_Event.bjet1->pt();
  E = alpha * m_Event.bjet1->e();
  jet_mass = std::sqrt(E * E - pt * pt * std::cosh(eta) * std::cosh(eta));
  xAOD::JetFourMom_t jet14vec(pt, eta, phi, jet_mass);

  m->GetParameter(1, alpha, err);
  m->GetParameter(3, eta, err);
  m->GetParameter(5, phi, err);
  pt = alpha * m_Event.bjet2->pt();
  E = alpha * m_Event.bjet2->e();
  jet_mass = std::sqrt(E * E - pt * pt * std::cosh(eta) * std::cosh(eta));
  xAOD::JetFourMom_t jet24vec(pt, eta, phi, jet_mass);

  // First iteration saves unbiased Mbb
  if(iteration==1){
    KF_Mbb = (jet14vec+jet24vec).M();
    return;
  }

  // Second iteration saves kinematics with Mbb constraint
  bjet1->setJetP4(jet14vec);
  bjet2->setJetP4(jet24vec);

  for(unsigned int i=0; i<extra_jets.size(); i++){
    m->GetParameter(6+i*3, alpha, err);
    m->GetParameter(7+i*3, eta, err);
    m->GetParameter(8+i*3, phi, err);
    pt = alpha * m_Event.extra_jets.at(i)->pt();
    E = alpha * m_Event.extra_jets.at(i)->e();
    jet_mass = std::sqrt(E * E - pt * pt * std::cosh(eta) * std::cosh(eta));
    xAOD::JetFourMom_t jet4vec(pt, eta, phi, jet_mass);
    extra_jets[i]->setJetP4(jet4vec);
  }

  return;
}
